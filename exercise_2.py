# Zadanie 2
# Napisz funkcję, która przyjmuje dwa parametry:
# ● rodzaj zwierzęcia
# ● wiek
# Funkcja zwraca informację czy zwierzę wymaga szczepienia.
# Wszystkie zwierzęta wymagają szczepienia w pierwszym roku życia.
# Zwierzęta wymagają ponownego szczepienia:
# ● koty co 3 lata
# ● psy co 2 lata
# Stestuj funkcję dla różnych przypadków.
# * Jeśli wiek lub rodzaj zwierzęcia jest niewspierany - rzuć błędem (https://realpython.com/python-raise-exception/)

def validate_animal_data(animal_kind, animal_age):
    if animal_age <= 0:
        raise ValueError("Animal age must be grater or equal to 1")
    elif animal_kind != "cat" and animal_kind != "dog":
        raise ValueError("Unknown animal kind")


def whether_the_animal_must_be_vaccinated(animal_kind: str, animal_age: int) -> bool:
    validate_animal_data(animal_kind, animal_age)

    if animal_age == 1:
        return True
    elif animal_kind == "cat" and (animal_age - 1) % 3 == 0:
        return True
    elif animal_kind == "dog" and (animal_age - 1) % 2 == 0:
        return True
    else:
        return False
#


if __name__ == '__main__':
    animal_1 = {
        "kind": "cat",
        "age": 1
    }

    animal_2 = {
        "kind": "cat",
        "age": 10
    }

    animal_3 = {
        "kind": "cat",
        "age": 12
    }

    animal_4 = {
        "kind": "dog",
        "age": 1
    }

    animal_5 = {
        "kind": "dog",
        "age": 11
    }

    animal_6 = {
        "kind": "dog",
        "age": 14
    }

    animal_7 = {
        "kind": "monkey",
        "age": 3
    }

    animal_8 = {
        "kind": "dog",
        "age": 0
    }

    animals = [animal_1, animal_2, animal_3, animal_4, animal_5, animal_6]
    for animal in animals:
        print(f"{animal["age"]}-year-old {animal["kind"]} must be vaccinated: {whether_the_animal_must_be_vaccinated(animal["kind"], animal["age"])}")

    # Raise animal kind Exception
    print(f"{animal_7["age"]}-year-old {animal_7["kind"]} must be vaccinated: {whether_the_animal_must_be_vaccinated(animal_7["kind"], animal_7["age"])}")

    # Raise animal age Exception
    print(f"{animal_8["age"]}-year-old {animal_8["kind"]} must be vaccinated: {whether_the_animal_must_be_vaccinated(animal_8["kind"], animal_8["age"])}")
