# Zadanie 1
# Utwórz funkcję, która przyjmuje słownik opisujący gracza:
# {
#  “nick”: “maestro_54”,
#  “type”: “warrior”,
#  “exp_points”: 3000
# }
# i zwraca jego opis w postaci:
# “The player maestro_54 is of type warrior and has 3000 EXP”

def print_player_description(player_dict: dict):
    print(f"The player {player_dict["nick"]} is of type {player_dict["type"]} and has {player_dict["exp_points"]} EXP")


if __name__ == '__main__':
    game_player = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }

    game_player_2 = {
        "nick": "kolo_365",
        "type": "magician",
        "exp_points": 2000
    }

    print_player_description(game_player)
    print_player_description(game_player_2)
